import React from 'react';
import {render} from 'react-dom';
import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import createHistory from 'history/createBrowserHistory'
import { Route } from 'react-router';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';

import comments from './reducers/commentReducer';
import App from './components/app';
import {loadComments} from './actions/commentActions';
import './styles/app.scss';

const history = createHistory();
const middleware = routerMiddleware(history)
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
	combineReducers({
		comments,
	  	router: routerReducer
	}),
	composeEnhancers(applyMiddleware(thunk, reduxImmutableStateInvariant(), middleware))
  );

store.dispatch(loadComments());

render (
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<Route path="/" component={App}/>
    	</ConnectedRouter>
	</Provider>, 
	document.getElementById('app')
	);