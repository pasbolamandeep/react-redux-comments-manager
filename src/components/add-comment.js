import React from 'react';
import PropTypes from 'prop-types';

const AddComment =({actions}) => {
	let titleInput = null;
	let descInput = null;
	const handleClick = () => {
		actions.addComment(titleInput.value, descInput.value)
	}
	return (
		<div className="add-comment">
			<input type="text" placeholder="Title" ref={(input) => { titleInput = input; }}/>
			<textarea placeholder="Description" ref={(input) => { descInput = input; }}>				
			</textarea>
			<button type="button" onClick={handleClick}>Add</button>
		</div>
	)
}

AddComment.propTypes = {
	actions: PropTypes.object.isRequired
}

export default AddComment;