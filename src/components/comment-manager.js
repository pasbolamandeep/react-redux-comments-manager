import React from 'react';
import PropTypes from 'prop-types';
import AddComment from './add-comment';
import Comment from './comment';

import * as commentActions from '../actions/commentActions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class CommentManager extends React.Component {	
	render() {		
		const {comments} = this.props;
		const actions = this.props.actions;
		return (
			<div className="container">
			<AddComment actions={actions}/>
			<div className="comments">
				{
					comments.map((comment, i) => (
						<Comment comment={comment} actions={actions} key={comment.id}/>
					))
				}			
			</div>
			</div>
		);
	}
}

CommentManager.propTypes = {
	comments: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
	return {
		comments: state.comments,
		actions: state.actions
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators(commentActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentManager)
