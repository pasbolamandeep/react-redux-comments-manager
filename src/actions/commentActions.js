import * as actionTypes from './actionTypes'
import commentsApi from '../api/commentsApi';

export const loadCommentsSuccess = (comments) => {
	return {'type':actionTypes.LOAD_COMMENTS_SUCCESS, comments}
}

export const loadComments = () => {
	return dispatch => {
		return commentsApi.getAllComments().then(comments => {
			dispatch(loadCommentsSuccess(comments));
		}).catch(error => {
			throw(error);
		})
	}
}

export const saveCommentSuccess = (comment) => {
	return {'type': actionTypes.SAVE_COMMENT_SUCCESS, comment}
}

export const saveComment = (id, title, desc) => {
	return dispatch => {
		return commentsApi.saveComment(id, title, desc).then(comment => {
			dispatch(saveCommentSuccess(comment));
		}).catch(error => {
			throw(error);
		})
	}
}

export const addCommentSuccess = (comment) => {
	return {'type': actionTypes.ADD_COMMENT_SUCCESS, comment}
}

export const addComment = (title, desc) => {
	return dispatch => {
		return commentsApi.addComment(title, desc).then(comment => {
			dispatch(addCommentSuccess(comment));
		}).catch(error => {
			throw(error);
		})
	}
}

export const deleteCommentSuccess = (id) => {
	return {'type': actionTypes.DELETE_COMMENT_SUCCESS, id}
}

export const deleteComment = (id) => {
	return dispatch => {
		return commentsApi.deleteComment(id).then(id => {
			dispatch(deleteCommentSuccess(id));
		}).catch(error => {
			throw(error);
		})
	}
}